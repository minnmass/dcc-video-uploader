Quick-and-dirty app I threw together to help DCC automate the render-and-upload-to-YouTube process for their pandemic-related video services.

The app needs write access to YouTube, to upload the video and thumbnail.

## Privacy policy, so the app can be verified ##

The app uses .NET, which may allow diagnostic data (eg., crash reports) to Microsoft in some circumstances; I haven't told it to do so, however.

The app, by itself, does not send any information to any entity other than Google; that information is limited to:

* credentials/login tokens, which are used solely to upload the videos and thumbnails to YouTube
* the videos, video title/descriptions, and thumbnails that the app uploads to YouTube

The app doesn't "phone home" in any way. I don't believe there's a way for me to see who has logged in with the app in Google's console, but that's the only way I'd get any information about people's use of this app without their initiating communication (eg., by posting a bug or opening a PR).