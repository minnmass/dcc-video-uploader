﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Microsoft.Win32;
using MimeKit;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace DCC_YouTubeUploader {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		private static long videoFileSize = 1;
		private static string videoId = null;

		public MainWindow() {
			Properties.Settings.Default.Save();
			InitializeComponent();

			Date.SelectedDate = GetNextWeekday(DayOfWeek.Sunday);

			UpdateDefaultVideoNameAndDescription(null, null);

			if (File.Exists(Properties.Settings.Default.LastMeltFile)) {
				MltLocation.Text = Properties.Settings.Default.LastMeltFile;
			}
			if (File.Exists(Properties.Settings.Default.LastThumbnailFile)) {
				ThumbnailLocation.Text = Properties.Settings.Default.LastThumbnailFile;
			}

			static DateTime GetNextWeekday(DayOfWeek day) {
				// The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
				var start = DateTime.Now;
				int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
				return start.AddDays(daysToAdd);
			}
		}

		private async void RunButtonClick(object sender, RoutedEventArgs e) {
			RunButton.IsEnabled = false;
			var output = new StringBuilder();

			await Run();

			OutputTextBox.Text = output.ToString();
			RunButton.IsEnabled = true;

			async Task Run() {
				if (String.IsNullOrWhiteSpace(MltLocation.Text) || !File.Exists(MltLocation.Text)) {
					output.AppendLine("No Mlt file selected");
				} else {
					string outputFile = null;
					try {
						var doc = new XmlDocument();
						doc.Load(MltLocation.Text);
						outputFile = doc.SelectSingleNode("/mlt/consumer")?.Attributes["target"]?.Value;
						if (outputFile is null) {
							output.AppendLine("Could not parse mlt file to determine where the video will be written out to.");
							return;
						}
						output.Append($"video file will be written to: ");
						output.AppendLine(outputFile);

						if (Render.IsChecked == true) {
							var meltTask = RunMelt(MltLocation.Text);
							OutputTextBox.Text = output.ToString();

							var meltSucceeded = await meltTask;

							if (!meltSucceeded) {
								output.AppendLine("Melt (rendering) task failed.");
								return;
							}
						}

						output.AppendLine("Video rendered; uploading to YouTube.");
						OutputTextBox.Text = output.ToString();
					} catch (Exception ex) {
						output.AppendLine("Something went wrong reading the mlt file.");
						output.AppendLine(ex.Message);
					}
					try {
						var youtubeUploadStatus = await YouTubeUpload(outputFile);
						if (youtubeUploadStatus.success) {
							output.AppendLine("Video uploaded successfully.");
						} else {
							output.AppendLine(youtubeUploadStatus.errorMessage);
						}
					} catch (Exception ex) {
						output.AppendLine("Something went wrong uploading to YouTube.");
						output.AppendLine(ex.Message);
					}
				}
			}
		}

		private void UpdateDefaultVideoNameAndDescription(object sender, RoutedEventArgs e) {
			string dateString = Date.SelectedDate.Value.ToString("MMMM d, yyyy");
			VideoName.Text = $"DCC {dateString}";
			VideoDescription.Text = $"Duluth Congregational Church{Environment.NewLine}{dateString}{Environment.NewLine}{Properties.Settings.Default.MinisterName}";
		}

		private void SelectMltFile(object sender, RoutedEventArgs e) {
			var dialog = new OpenFileDialog {
				Filter = "Mlt files (*.mlt)|*.mlt|All files (*.*)|*.*"
			};
			if (Directory.Exists(Properties.Settings.Default.LastMeltDirectory)) {
				dialog.InitialDirectory = Properties.Settings.Default.LastMeltDirectory;
			}
			if (dialog.ShowDialog() == true) {
				MltLocation.Text = dialog.FileName;
				Properties.Settings.Default.LastMeltFile = dialog.FileName;
				Properties.Settings.Default.LastMeltDirectory = Path.GetDirectoryName(dialog.FileName);
				Properties.Settings.Default.Save();
			}
		}

		private async Task<(bool success, string errorMessage)> YouTubeUpload(string videoFile) {
			UserCredential credential;
			using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read)) {
				credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets,
					// This OAuth 2.0 access scope allows an application to upload files to the
					// authenticated user's YouTube channel, but doesn't allow other types of access.
					new[] { YouTubeService.Scope.YoutubeUpload },
					"user",
					CancellationToken.None
				);
			}

			var youtubeService = new YouTubeService(new BaseClientService.Initializer() {
				HttpClientInitializer = credential,
				ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
			});

			var video = new Video {
				Snippet = new VideoSnippet {
					Title = VideoName.Text,
					Description = VideoDescription.Text,
					Tags = Array.Empty<string>(),
					ChannelId = Properties.Settings.Default.ChannelId,
					CategoryId = "22" // See https://developers.google.com/youtube/v3/docs/videoCategories/list
				},
				Status = new VideoStatus {
					PrivacyStatus = "private" // or "unlisted" or "public"
				}
			};


			var fileInfo = new FileInfo(videoFile);
			videoFileSize = fileInfo.Length;

			using (var fileStream = new FileStream(videoFile, FileMode.Open)) {
				var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
				videosInsertRequest.ProgressChanged += VideosInsertRequest_ProgressChanged;
				videosInsertRequest.ResponseReceived += VideosInsertRequest_ResponseReceived;

				var response = await videosInsertRequest.UploadAsync();

				if (response.Status != UploadStatus.Completed) {
					return new(false, $"Video upload failed:{Environment.NewLine}{response.Exception.Message}");
				}
			}

			if (!String.IsNullOrWhiteSpace(ThumbnailLocation.Text) && File.Exists(ThumbnailLocation.Text)) {
				const int youtubeMaxWidth = 1280;
				const int youtubeMaxHeight = 720;
				var image = Image.FromFile(ThumbnailLocation.Text);
				var originalFormat = image.RawFormat;
				if (image.Width > youtubeMaxWidth || image.Height > youtubeMaxHeight) {
					image = ResizeImage(image, youtubeMaxWidth, youtubeMaxHeight);
				}

				using (var imageStream = new MemoryStream()) {
					image.Save(imageStream, originalFormat);
					imageStream.Position = 0;

					var response = await youtubeService.Thumbnails.Set(videoId, imageStream, MimeTypes.GetMimeType(ThumbnailLocation.Text)).UploadAsync();
					if (response.Status != UploadStatus.Completed) {
						return new(false, $"Thumbnail upload failed:{Environment.NewLine}{response.Exception.Message}");
					}
				}
			}

			return new(true, null);

			void VideosInsertRequest_ProgressChanged(IUploadProgress progress) {
				switch (progress.Status) {
					case UploadStatus.Uploading:
						Dispatcher.Invoke(() =>
							OutputTextBox.Text = $"Uploaded {progress.BytesSent} of {videoFileSize} bytes ({Decimal.Round(progress.BytesSent / (decimal)videoFileSize * 100, 2)}%)."
						);
						break;
				}
			}

			void VideosInsertRequest_ResponseReceived(Video video) {
				videoId = video.Id;
			}

			static Bitmap ResizeImage(Image image, int width, int height) {
				var destRect = new Rectangle(0, 0, width, height);
				var destImage = new Bitmap(width, height);

				destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

				using (var graphics = Graphics.FromImage(destImage)) {
					graphics.CompositingMode = CompositingMode.SourceCopy;
					graphics.CompositingQuality = CompositingQuality.HighQuality;
					graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
					graphics.SmoothingMode = SmoothingMode.HighQuality;
					graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

					using (var wrapMode = new ImageAttributes()) {
						wrapMode.SetWrapMode(WrapMode.TileFlipXY);
						graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
					}
				}

				return destImage;
			}
		}

		private static async Task<bool> RunMelt(string meltFile) {
			ProcessStartInfo psi = new ProcessStartInfo(Properties.Settings.Default.MeltExecutable, $"-progress {meltFile}") {
				UseShellExecute = false,
				RedirectStandardOutput = false,
				RedirectStandardInput = false,
				RedirectStandardError = false
			};
			Process proc = Process.Start(psi);
			await proc.WaitForExitAsync();
			return proc.ExitCode == 0;
		}

		private void SelectThumbnailFile(object sender, RoutedEventArgs e) {
			var dialog = new OpenFileDialog() {
				Filter = "Image Files(*.PNG;*.JPG;*.GIF)|*.PNG;*.JPG;*.GIF"
			};
			if (Directory.Exists(Properties.Settings.Default.LastThumbnailDirectory)) {
				dialog.InitialDirectory = Properties.Settings.Default.LastThumbnailDirectory;
			}
			if (dialog.ShowDialog() == true) {
				ThumbnailLocation.Text = dialog.FileName;
				Properties.Settings.Default.LastThumbnailFile = dialog.FileName;
				Properties.Settings.Default.LastThumbnailDirectory = Path.GetDirectoryName(dialog.FileName);
				Properties.Settings.Default.Save();
			}
		}

		private void UpdateAdvancedSettings(object sender, RoutedEventArgs e) {
			new SettingsWindow().Show();
		}
	}
}
