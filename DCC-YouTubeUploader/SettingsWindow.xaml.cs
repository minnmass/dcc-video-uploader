﻿using Microsoft.Win32;
using System.Windows;

namespace DCC_YouTubeUploader {
	/// <summary>
	/// Interaction logic for SettingsWindow.xaml
	/// </summary>
	public partial class SettingsWindow : Window {
		public SettingsWindow() {
			InitializeComponent();

			MeltExecutableConfig.Text = Properties.Settings.Default.MeltExecutable;
			MinisterNameConfig.Text = Properties.Settings.Default.MinisterName;
			ChannelIdConfig.Text = Properties.Settings.Default.ChannelId;
		}

		private void SelectMeltExecutableLocation(object sender, RoutedEventArgs e) {
			var dialog = new OpenFileDialog {
				Filter = "Executable files|*.exe"
			};
			if (dialog.ShowDialog() == true) {
				MeltExecutableConfig.Text = dialog.FileName;
			}
		}

		private void SaveSettingsButton(object sender, RoutedEventArgs e) {
			Properties.Settings.Default.MeltExecutable = MeltExecutableConfig.Text;
			Properties.Settings.Default.MinisterName = MinisterNameConfig.Text;
			Properties.Settings.Default.ChannelId = ChannelIdConfig.Text;

			Properties.Settings.Default.Save();
			Close();
		}
	}
}
